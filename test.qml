import QtQuick 2.0

Rectangle {
    id: root
    width: 300
    height: 400

    state: mouseArea.pressed ? "expanded" : ""

    NotchLayout {
        id: layout
        anchors {
            left: parent.left; right: parent.right
            top: parent.top
        }

        availableSegments: [[0, 100], [120, 190], [210, 300]]

        TestItem {
            color: "blue"
            normalWidth: 20
            expandedWidth: 40
            state: root.state
        }

        TestItem {
            color: "green"
            normalWidth: 30
            expandedWidth: 45
            state: root.state
        }

        TestItem {
            color: "red"
            normalWidth: 0
            expandedWidth: 40
            state: root.state
        }

        TestItem {
            color: "yellow"
            normalWidth: 20
            expandedWidth: 35
            state: root.state
        }

        TestItem {
            color: "pink"
            normalWidth: 40
            expandedWidth: 45
            state: root.state
        }

        TestItem {
            color: "red"
            normalWidth: 0
            expandedWidth: 30
            state: root.state
        }

        TestItem {
            color: "#4f4"
            normalWidth: 20
            expandedWidth: 40
            state: root.state
        }

        TestItem {
            color: "red"
            normalWidth: 0
            expandedWidth: 25
            state: root.state
        }
    }

    Rectangle {
        id: notch1
        color: "#222"
        radius: 10
        x: 100
        y: 2
        width: 20
        height: 20
    }

    Rectangle {
        id: notch2
        color: "#222"
        radius: 10
        x: 190
        y: 2
        width: 20
        height: 20
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onPositionChanged: {
            layout.ensureVisible(layout.itemAt(mouse.x))
        }
    }
}
