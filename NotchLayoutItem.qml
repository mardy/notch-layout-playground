import QtQuick 2.0

Item {
    id: root
    implicitWidth: children[0].implicitWidth
    implicitHeight: children[0].implicitHeight

    Behavior on x {
        NumberAnimation { duration: 200 }
    }
}
