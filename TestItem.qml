import QtQuick 2.0

NotchLayoutItem {
    id: root

    property int normalWidth: 0
    property int expandedWidth: 0
    property alias color: rect.color

    Rectangle {
        id: rect
        anchors.fill: parent
        implicitWidth: root.state == "expanded" ? root.expandedWidth : root.normalWidth
        implicitHeight: 15; y: 0
    }
}
