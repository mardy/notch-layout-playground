import QtQuick 2.0
import QtQml 2.4

Item {
    id: root

    default property alias data: itemsHolder.data

    property var availableSegments: []
    property int pixelPerSecond: 150

    property var watchedChildren: itemsHolder.visibleChildren

    Component.onCompleted: computeRestLayout()

    Instantiator {
        model: watchedChildren
        Connections {
            target: model
            onWidthChanged: root.computeRestLayout()
        }
    }

    Item {
        id: itemsHolder
        anchors.fill: parent
    }

    function segmentWidth(index) {
        if (index < 0 || index > availableSegments.length - 1) return 0
        var segment = availableSegments[index]
        return segment[1] - segment[0]
    }

    function computeRestLayout() {
        console.log("Recomputing layout")
        var items = itemsHolder.visibleChildren

        var firstAvailableSegment = availableSegments.length - 1
        var availableSpaceInSegment = segmentWidth(firstAvailableSegment)

        for (var i = 0; i < items.length; i++) {
            if (firstAvailableSegment < 0) break

            // we want to right-align them, so start from the last item
            var item = items[items.length - 1 - i]
            var itemWidth = item.implicitWidth > 0 ? item.implicitWidth : item.width
            while (itemWidth > availableSpaceInSegment) {
                console.log("No space in segment " + firstAvailableSegment + " for item " + item.color)
                firstAvailableSegment--
                if (firstAvailableSegment < 0) return
                availableSpaceInSegment = segmentWidth(firstAvailableSegment)
                console.log("Space available in next segment: " + availableSpaceInSegment)
            }
            var segmentLeft = availableSegments[firstAvailableSegment][0]
            console.log("Segment start: " + segmentLeft)
            item.x = segmentLeft + availableSpaceInSegment - itemWidth
            availableSpaceInSegment -= itemWidth
        }
    }

    function itemAt(x) {
        return childAt(x, height / 2)
    }

    function ensureVisible(item) {
    }
}
